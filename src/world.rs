extern crate derive_more;
use derive_more::{Display, From, Into, Error };
use hex;
use byteorder::*;
use tinyvec::{ ArrayVec, array_vec};
use custom_error::{custom_error};

#[derive(Debug, Default)]
pub struct World {
    wt: WorldType,          // Offset: 0
    num_boards: i16,        // Offset: 2
    ammo: i16,              // Offset: 4
    gems: i16,              // Offset: 6 
    keys: ArrayVec<[i8; 7]>,// Offset: 8
    health: i16,            // Offset 15
    cur_board: i16,         // OFfset: 17
}

impl World {
    pub fn new(bytes: &[u8]) -> Result<Self, WorldError> {
        let wt = match bytes[0..1] {
            [0xFF, 0xFF] => WorldType::ZZT,
            [0xFE, 0xFF] => WorldType::SuperZZT,
            [0xFD, 0xFF] => WorldType::RZT,
            _ => return Err(WorldError::InvalidWorldType)
        };

        // Get all the simple things.
        let num_boards = LE::read_i16(&bytes[2..3]);
        let ammo = LE::read_i16(&bytes[4..5]);

        Ok(Self {
            wt,
            num_boards,
            ..Default::default()
        })
    }
}

#[derive(Debug)]
pub enum WorldType {
    ZZT,                // FF FF
    SuperZZT,           // FE FF
    RZT,                // FD FF
}

/// Defaults to regular ZZT boards.
impl Default for WorldType {
    fn default() -> Self {
        WorldType::ZZT
    }
}

pub struct Board {

}

custom_error! {
    pub WorldError 
        InvalidWorldType = "World type is not recognized.",   
}