pub mod load;
pub mod world;

#[cfg(test)]
mod tests {
    use super::*;
    use world::World as World;
    #[test]
    fn can_read_zzt_world() {
        let zzt_file = include_bytes!("../test_assets/TOWN.ZZT");

        let w = World::new(zzt_file);
    }
}
